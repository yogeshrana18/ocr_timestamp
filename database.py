from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from commons import find_mysql_server, get_pass, find_oracle_server, get_oracle_pass
from finstinct_config.loader import FinstinctConfig

mysql_creds = find_mysql_server()
oracle_creds = find_oracle_server()
# --------------------------------- DB Config---------------------------------------------------------------
config_obj = FinstinctConfig.get_config()
kengine_db = config_obj.db_config.kengine_db

engine = None
if kengine_db == 'mysql':
    sqlalchemy_database_url = "mysql+pymysql://" + mysql_creds['user'] + ":" + get_pass() + "@" + mysql_creds[
        'host'] + "/" + mysql_creds['db'] + "?host=" + mysql_creds['host'] + "?port=" + mysql_creds['port']

    engine = create_engine(
        sqlalchemy_database_url,
        pool_recycle=-1,
        pool_pre_ping=True
    )
elif kengine_db == 'oracle':
    # TODO change oracle url
    sqlalchemy_database_url = "oracle+cx_oracle://" + oracle_creds['user'] + ":" + get_oracle_pass() + "@" + \
                              oracle_creds['host'] + ":" + oracle_creds['port'] + "/" + oracle_creds['db'] + ""

    engine = create_engine(
        sqlalchemy_database_url,
        pool_recycle=-1,
        pre_pool_pre_ping=True,
        max_identifier_length=128
    )

_SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

def get_session():
    return _SessionLocal()