import base64
import os
from codecs import decode
from subprocess import check_output


def get_pass():
    passfile = os.path.expanduser("~/mysql.o")
    # if not os.path.exists(passfile):
        # passfile = "/root/mysql.o"
    if not os.path.exists(passfile) and "JAVA_HOME" in os.environ:
        JAVA_HOME = os.environ["JAVA_HOME"]
        if JAVA_HOME.endswith("/"):
            JAVA_HOME = JAVA_HOME[:-1]
        passfile = JAVA_HOME + "/" + "mysql.o"
    return base64.decodebytes(decode(check_output([passfile]).decode("utf-8"), "rot13").encode()).decode("utf-8")


def get_oracle_pass():
    passfile = os.path.expanduser("~/oracle.o")
    if not os.path.exists(passfile) and "JAVA_HOME" in os.environ:
        JAVA_HOME = os.environ["JAVA_HOME"]
        if JAVA_HOME.endswith("/"):
            JAVA_HOME = JAVA_HOME[:-1]
        passfile = JAVA_HOME + "/" + "oracle.o"
    return base64.decodebytes(decode(check_output([passfile]).decode("utf-8"), "rot13").encode()).decode("utf-8")


def find_mysql_server():
    config_file = os.path.expanduser("~/mysql_server.ini")
    if not os.path.exists(config_file) and "JAVA_HOME" in os.environ:
        JAVA_HOME = os.environ["JAVA_HOME"]
        if JAVA_HOME.endswith("/"):
            JAVA_HOME = JAVA_HOME[:-1]
        config_file = JAVA_HOME + "/" + "mysql_server.ini"
    with open(config_file, "r") as config_fp:
        lines = config_fp.readlines()
        return dict([[item.strip() for item in l.strip().split("=")] for l in lines])


def find_oracle_server():
    config_file = os.path.expanduser("~/oracle_server.ini")
    if not os.path.exists(config_file) and "JAVA_HOME" in os.environ:
        JAVA_HOME = os.environ["JAVA_HOME"]
        if JAVA_HOME.endswith("/"):
            JAVA_HOME = JAVA_HOME[:-1]
        config_file = JAVA_HOME + "/" + "oracle_server.ini"
    with open(config_file, "r") as config_fp:
        lines = config_fp.readlines()
        return dict([[item.strip() for item in l.strip().split("=")] for l in lines])


def find_mongo_server():
    config_file = os.path.expanduser("~/mongo_server.ini")
    if not os.path.exists(config_file) and "JAVA_HOME" in os.environ:
        JAVA_HOME = os.environ["JAVA_HOME"]
        if JAVA_HOME.endswith("/"):
            JAVA_HOME = JAVA_HOME[:-1]
        config_file = JAVA_HOME + "/" + "mongo_server.ini"
    with open(config_file, "r") as config_fp:
        lines = config_fp.readlines()
        return dict([[item.strip() for item in l.strip().split("=")] for l in lines])
