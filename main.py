import datetime
import json
from flask import Flask, request
from database import get_session
app = Flask(__name__)


@app.route('/fetch_ocr_timestamp/', methods=['POST'])
def fetch_ocr_timestamp():
    if request.json.get('status') != 'OCR DONE':
        return ''
    # add ocr time
    session = get_session()
    #print('processing request.....',request.json,flush=True)
    doc_ref_id = request.json["job_id"]
    #print('doc_ref_id:::',doc_ref_id)
    result = session.execute("select request_time, request_params from QButlerTasks where document_ref_id='{}'".format(doc_ref_id))
    start_time, qbutler_req_params = result.fetchone()
    filename = json.loads(qbutler_req_params)['filename'].split('/')[-1]
    print("Received job with file:: ",filename, "with start time", start_time)
    inter_diff = datetime.datetime.now() - start_time
    print("Processed job with file:: ",filename," to compute OCR Confidence: ",request.json['ocr_confidence'], " with intermediate time difference in seconds::", inter_diff.seconds, flush=True)
    print(" ------------------------------ Process Finished ---------------------------------", flush=True)
    session.execute("update QButlerTasks set intermediate_time ='{}' where document_ref_id='{}'".format(datetime.datetime.now(), doc_ref_id))
    session.commit()
    return 'Done.'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5088)
    #app.run(port=5018)